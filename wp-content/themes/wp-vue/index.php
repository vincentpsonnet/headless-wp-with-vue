<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>

	<div id="app">

		<?php get_template_part('/templates/home'); ?>

	</div>

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/vue.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/vue-resource.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/vue-router.js"></script>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/app.js"></script>
</body>
</html>
