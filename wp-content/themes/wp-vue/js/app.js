const url = 'index.php/wp-json/wp/v2/posts';

// Components with routes
const Home = Vue.component('home', {
	template: '#home'
});

// Routes list
const routes = [
	{ 
		path: '/', 
		component: Home 
	}
];

// Router Declaration
const router = new VueRouter({
	routes: routes
});


// App iniatation
var app = new Vue({
	el: '#app',
	router: router,
	
	data: function(){
		return {
			posts: []
		}
	},

	mounted: function(){
		this.getPosts();
	},

	methods: {
		getPosts: function() {
			var _this = this;
			_this.$http.get(url).then((response) => { 
				console.log(response);
				_this.posts = response.body; 
			});
		}

	}
});