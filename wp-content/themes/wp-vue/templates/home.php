<template id="home">

	<div class="container">
		<div class="row">
			<article v-for="post in posts" class="post col-12 col-sm-4">

				<div class="card">
					<div class="card-body">
						<h2 class="card-header">{{ post.title.rendered }}</h2>
						<div v-html="post.excerpt.rendered" class="card-content"></div>
					</div>   
				</div>
				
	        </article>
		</div>
	</div>	

</template>